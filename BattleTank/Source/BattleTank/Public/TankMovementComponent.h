// Copyright Bernhard St�ger

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/NavMovementComponent.h"
#include "TankMovementComponent.generated.h"

class UTankTrack;

/**
 * Responsible for driving the tank tracks
 */
UCLASS( ClassGroup = (Custom), meta = (BlueprintSpawnableComponent) )
class BATTLETANK_API UTankMovementComponent : public UNavMovementComponent
{
	GENERATED_BODY()

public:
	UFUNCTION(BlueprintCallable, Category = "Input")
	void IntendMoveForward(float controlThrow);

	UFUNCTION(BlueprintCallable, Category = "Input")
	void IntendTurnRight(float controlThrow);

	UFUNCTION(BlueprintCallable, Category = "Setup")
	void Initialise(UTankTrack* leftTrackToSet, UTankTrack* rightTrackToSet);

private:
	UTankTrack* leftTrack = nullptr;
	UTankTrack* rightTrack = nullptr;

	// Called from the pathfinding logic by the AI controllers
	virtual void RequestDirectMove(const FVector& MoveVelocity, bool bForceMaxSpeed) override;
};
