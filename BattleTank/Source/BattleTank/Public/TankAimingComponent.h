// Copyright Bernhard St�ger

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "TankAimingComponent.generated.h"

class AProjectile;

// Enum for aiming state
UENUM()
enum class EFiringState : uint8
{
	Reloading,
	Aiming,
	Locked,
	OutOfAmmo
};

class UTankBarrel;
class UTankTurret;

/*
* Responsible for aiming 
*/
UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class BATTLETANK_API UTankAimingComponent : public UActorComponent
{
	GENERATED_BODY()

public:
	UFUNCTION(BlueprintCallable, Category = "Setup")
	void Initialise(UTankBarrel* barrelToSet, UTankTurret* turretToSet);

	void AimAt(FVector hitLocation);

	UFUNCTION(BlueprintCallable, Category = "Firing")
	void Fire();

	EFiringState GetFiringState();

	UFUNCTION(BlueprintCallable, Category = "Firing")
	int32 GetCurrentAmmo() const;

protected:
	UPROPERTY(BlueprintReadOnly, Category = "State")
	EFiringState m_firingState = EFiringState::Reloading;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Setup")
	TSubclassOf<AProjectile> m_projectile;
private:
	// Sets default values for this component's properties
	UTankAimingComponent();

	virtual void BeginPlay() override;
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

	bool IsBarrelMoving();

	void MoveBarrelTowards(FVector aimDirection);

	UPROPERTY(EditDefaultsOnly, Category = "Firing")
	float m_launchSpeed = 4000;

	UPROPERTY(EditDefaultsOnly, Category = "Firing")
	float m_reloadTimeInSeconds = 3.0f;

	UPROPERTY(EditDefaultsOnly, Category = "Firing")
	int32 m_maxAmmo = 5;

	FVector m_aimDirection;

	double m_lastFileTime = 0.0;

	int32 m_currentAmmo;

	// Local barrel reference for spawning a projectile
	UTankBarrel* m_barrel = nullptr;
	UTankTurret* m_turret = nullptr;
};
