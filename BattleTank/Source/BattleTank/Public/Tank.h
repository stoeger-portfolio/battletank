// Copyright Bernhard St�ger

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Pawn.h"
#include "Tank.generated.h"

class UTankBarrel;

DECLARE_DYNAMIC_MULTICAST_DELEGATE(FTankDies);

UCLASS()
class BATTLETANK_API ATank : public APawn
{
	GENERATED_BODY()

public:
	// Returns current health as a percentage of starting health, between 0 and 1
	UFUNCTION(BlueprintPure, Category = "Health")
	float GetHealthPercent() const;

	UPROPERTY()
	FTankDies OnDeath;
private:
	ATank();

	virtual float TakeDamage(float Damage, struct FDamageEvent const& DamageEvent, AController* EventInstigator, AActor* DamageCauser) override;
	virtual void BeginPlay() override;

	UPROPERTY(EditDefaultsOnly, Category = "Setup")
	int32 m_maxHealth = 100.0f;

	UPROPERTY(VisibleAnywhere, Category = "Health")
	int32 m_currentHealth = 100.0f;
};
