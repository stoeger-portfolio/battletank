// Copyright Bernhard St�ger

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/PlayerController.h"
#include "TankPlayerController.generated.h"

/**
 * Responsible for helping the player aim
 */
UCLASS()
class BATTLETANK_API ATankPlayerController : public APlayerController
{
	GENERATED_BODY()

protected:
	UFUNCTION(BlueprintImplementableEvent, Category = "Setup")
	void FoundAimingComponent(UTankAimingComponent* aimingComponentReference);

private:
	virtual void BeginPlay() override;
	virtual void Tick(float DeltaTime) override;
	virtual void SetPawn(APawn* InPawn) override;

	/** Starts moving the tanks barrel so that a show would hit where the crosshair intersects the world **/
	void AimTowardsCrosshair();
	bool GetSigthRayHitLocation(FVector& out_HitLocation) const;
	bool GetLookDirection(FVector2D screenLocation, FVector& out_LookDirection) const;
	bool GetLookVectorHitLocation(FVector lookDirection, FVector& out_result) const;

	UPROPERTY(EditDefaultsOnly)
	float m_CrosshairLocationX = 0.5f;

	UPROPERTY(EditDefaultsOnly)
	float m_CrosshairLocationY = 0.33333f;

	UPROPERTY(EditDefaultsOnly)
	float m_lineTraceRange = 1000000.0f;

	UFUNCTION()
	void OnTankDeath();
};
