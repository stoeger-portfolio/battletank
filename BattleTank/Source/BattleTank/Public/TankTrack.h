// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/StaticMeshComponent.h"
#include "TankTrack.generated.h"

class ASprungWheel;

/**
 * TankTrack is used to set maximum diving force, and to apply forces to the tank.
 */
UCLASS(ClassGroup = (Custom), meta = (BlueprintSpawnableComponent))
class BATTLETANK_API UTankTrack : public UStaticMeshComponent
{
	GENERATED_BODY()
	
public:
	// Set Throttle between 0 and 1
	UFUNCTION(BlueprintCallable, Category = "Input")
	void SetThrottle(float throttle);

private:
	UTankTrack();

	// This is max force per track in Newtons
	UPROPERTY(EditDefaultsOnly, Category = "Setup")
	float m_maximumDrivingForce = 40000000.0f; // Assume 40 tonne tank, and 1g acceleration
		
	void DriveTrack(float throttle);

	TArray<ASprungWheel*> GetWheels() const;
};
