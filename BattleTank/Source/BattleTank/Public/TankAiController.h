// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "AIController.h"
#include "TankAiController.generated.h"

/**
 * 
 */
UCLASS()
class BATTLETANK_API ATankAiController : public AAIController
{
	GENERATED_BODY()

private:
	virtual void Tick(float DeltaTime) override;

	virtual void SetPawn(APawn* InPawn) override;

	// How close can the AI tank get
	UPROPERTY(EditDefaultsOnly)
	float m_acceptanceRadius = 80000.0f;

	UFUNCTION()
	void OnTankDeath();
};
