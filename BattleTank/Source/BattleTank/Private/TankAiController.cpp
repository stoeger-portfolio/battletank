// Fill out your copyright notice in the Description page of Project Settings.


#include "TankAiController.h"
#include "Engine/Engine.h"
#include "TankAimingComponent.h"
#include "Tank.h"
#include "GameFramework/Pawn.h"
#include "Kismet/GameplayStatics.h"

void ATankAiController::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	APawn* controlledTank = GetPawn();
	APawn* playerTank = GetWorld()->GetFirstPlayerController()->GetPawn();

	if (!(playerTank && controlledTank)) { return; }
	
	MoveToActor(playerTank, m_acceptanceRadius);
	UTankAimingComponent* tankAimingComponent = GetPawn()->FindComponentByClass<UTankAimingComponent>();
	tankAimingComponent->AimAt(playerTank->GetActorLocation());

	if (tankAimingComponent->GetFiringState() == EFiringState::Locked)
	{
		tankAimingComponent->Fire();
	}
}

void ATankAiController::OnTankDeath()
{
	APawn* posessedTank = GetPawn();
	if (!posessedTank) { return; }
	posessedTank->DetachFromControllerPendingDestroy();
}

void ATankAiController::SetPawn(APawn* InPawn)
{
	Super::SetPawn(InPawn);

	if (InPawn)
	{
		ATank* posessedTank = Cast<ATank>(InPawn);
		if (!ensure(posessedTank)) { return; }
		
		posessedTank->OnDeath.AddUniqueDynamic(this, &ATankAiController::OnTankDeath);
	}
}
