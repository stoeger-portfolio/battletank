// Copyright Bernhard St�ger


#include "Tank.h"

float ATank::GetHealthPercent() const
{
	return (float)m_currentHealth / (float)m_maxHealth;
}

// Sets default values
ATank::ATank()
{
 	// Set this pawn to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = false;
}

float ATank::TakeDamage(float Damage, struct FDamageEvent const& DamageEvent, AController* EventInstigator, AActor* DamageCauser)
{
	int32 damagePoints = FPlatformMath::RoundToInt(Damage);
	int32 damageToApply = FMath::Clamp<int32>(damagePoints, 0, m_currentHealth);
	UE_LOG(LogTemp, Warning, TEXT("Damage: %f"), Damage);
	UE_LOG(LogTemp, Warning, TEXT("DamageToApply: %i"), damageToApply);

	m_currentHealth -= damageToApply;
	if (m_currentHealth <= 0)
	{
		OnDeath.Broadcast();
		UE_LOG(LogTemp, Warning, TEXT("Tank Dieded"));
	}

	return damageToApply;
}

void ATank::BeginPlay()
{
	Super::BeginPlay();
	m_currentHealth = m_maxHealth;
}
