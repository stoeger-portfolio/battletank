// Copyright Bernhard St�ger

#include "TankPlayerController.h"
#include "TankAimingComponent.h"
#include "Tank.h"
#include "Engine/World.h"

void ATankPlayerController::BeginPlay()
{
	Super::BeginPlay();
	if (!GetPawn()) { return; }
	UTankAimingComponent* tankAimingComponent = GetPawn()->FindComponentByClass<UTankAimingComponent>();
	if (!ensure(tankAimingComponent)) { return; }

	FoundAimingComponent(tankAimingComponent);
}

void ATankPlayerController::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	AimTowardsCrosshair();
}

void ATankPlayerController::SetPawn(APawn* InPawn)
{
	Super::SetPawn(InPawn);

	ATank* posessedTank = Cast<ATank>(InPawn);
	if (!ensure(posessedTank)) { return; }

	posessedTank->OnDeath.AddUniqueDynamic(this, &ATankPlayerController::OnTankDeath);
}

void ATankPlayerController::AimTowardsCrosshair()
{
	if (!GetPawn()) { return; } // e.g. if not posessing 

	UTankAimingComponent* tankAimingComponent = GetPawn()->FindComponentByClass<UTankAimingComponent>();
	if (!ensure(tankAimingComponent)) { return; }

	FVector hitLocation; // Out parameter
	if (GetSigthRayHitLocation(hitLocation)) // Has "side-effect", is going to line trace
	{
		tankAimingComponent->AimAt(hitLocation);
	}
}

// Get world location of linetrace through crosshair, true if hits landscape
bool ATankPlayerController::GetSigthRayHitLocation(FVector& out_HitLocation) const
{
	// Find the crosshair position
	int32 viewportSizeX;
	int32 viewportSizeY;

	GetViewportSize(viewportSizeX, viewportSizeY);

	FVector2D screenLocation(viewportSizeX * m_CrosshairLocationX, viewportSizeY * m_CrosshairLocationY);
	FVector lookDirection;
	if (GetLookDirection(screenLocation, lookDirection)) 
	{
		// Line-trace along that LookDirection, and see what we hit (up to max range)
		return GetLookVectorHitLocation(lookDirection, out_HitLocation);
	}

	return false;
}

bool ATankPlayerController::GetLookDirection(FVector2D screenLocation, FVector& out_LookDirection) const
{
	FVector cameraWorldLocation; // To be discarded

	// "De-project" the screen position of the crosshair to a world direction
	return DeprojectScreenPositionToWorld(screenLocation.X, screenLocation.Y, cameraWorldLocation, out_LookDirection);
}

bool ATankPlayerController::GetLookVectorHitLocation(FVector lookDirection, FVector& out_result) const
{
	FHitResult hit;
	FVector start = PlayerCameraManager->GetCameraLocation();
	FVector end = start + (lookDirection * m_lineTraceRange);
	if (GetWorld()->LineTraceSingleByChannel(hit, start, end, ECollisionChannel::ECC_Camera))
	{
		out_result = hit.Location;
		return true;
	}
	out_result = FVector(0.0f);
	return false;
}

void ATankPlayerController::OnTankDeath()
{
	StartSpectatingOnly();
}
