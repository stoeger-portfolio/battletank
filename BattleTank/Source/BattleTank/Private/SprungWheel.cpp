// Copyright Bernhard Stöger

#include "SprungWheel.h"
#include "PhysicsEngine/PhysicsConstraintComponent.h"
#include "Components/SphereComponent.h"

// Sets default values
ASprungWheel::ASprungWheel()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	PrimaryActorTick.TickGroup = TG_PostPhysics;

	springConstraint = CreateDefaultSubobject<UPhysicsConstraintComponent>(FName("Spring Constraint"));
	SetRootComponent(springConstraint);

	axle = CreateDefaultSubobject<USphereComponent>(FName("Axle"));
	axle->SetupAttachment(springConstraint);

	wheel = CreateDefaultSubobject<USphereComponent>(FName("Wheel"));
	wheel->SetupAttachment(axle);

	wheelConstraint = CreateDefaultSubobject<UPhysicsConstraintComponent>(FName("Wheel Constraint"));
	wheelConstraint->SetupAttachment(axle);
}

void ASprungWheel::SetupConstraint()
{
	if (!GetAttachParentActor()) { return; }

	UPrimitiveComponent* bodyRoot = Cast<UPrimitiveComponent>(GetAttachParentActor()->GetRootComponent());

	if (!bodyRoot) { return; }

	springConstraint->SetConstrainedComponents(bodyRoot, NAME_None, axle, NAME_None);
	wheelConstraint->SetConstrainedComponents(axle, NAME_None, wheel, NAME_None);
}

void ASprungWheel::OnHit(UPrimitiveComponent* HitComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, FVector NormalImpulse, const FHitResult& Hit)
{
	ApplyForce();
}

void ASprungWheel::ApplyForce()
{
	wheel->AddForce(axle->GetForwardVector() * m_totalForceMagnitudeThisFram);
}

void ASprungWheel::AddDrivingForce(float forceMagnitude)
{
	m_totalForceMagnitudeThisFram += forceMagnitude;
}

// Called when the game starts or when spawned
void ASprungWheel::BeginPlay()
{
	Super::BeginPlay();

	wheel->SetNotifyRigidBodyCollision(true);
	wheel->OnComponentHit.AddDynamic(this, &ASprungWheel::OnHit);

	SetupConstraint();
}

// Called every frame
void ASprungWheel::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	if (GetWorld()->TickGroup == TG_PostPhysics) 
	{
		m_totalForceMagnitudeThisFram = 0.0f;
	}
}

