// Fill out your copyright notice in the Description page of Project Settings.


#include "TankBarrel.h"

void UTankBarrel::Elevate(float relativeSpeed)
{
	relativeSpeed = FMath::Clamp<float>(relativeSpeed, -1.0f, 1.0f);
	float elevationChange = relativeSpeed * m_maxDegreesPerSecond * GetWorld()->DeltaTimeSeconds;
	float rawNewElevation = RelativeRotation.Pitch + elevationChange;

	float clampedElevation = FMath::Clamp<float>(rawNewElevation, m_minElevation, m_maxElevation);

	SetRelativeRotation(FRotator(clampedElevation, 0.0f, 0.0f));
}
