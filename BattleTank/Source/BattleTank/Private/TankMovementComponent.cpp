// Copyright Bernhard St�ger

#include "TankMovementComponent.h"
#include "TankTrack.h"

void UTankMovementComponent::Initialise(UTankTrack* leftTrackToSet, UTankTrack* rightTrackToSet)
{
	if (!ensure(leftTrackToSet && rightTrackToSet)) { return; }

	leftTrack = leftTrackToSet;
	rightTrack = rightTrackToSet;
}

void UTankMovementComponent::RequestDirectMove(const FVector& MoveVelocity, bool bForceMaxSpeed)
{
	// No need to call super! Replacing the functionality!
	FVector AiForwardIntention = MoveVelocity.GetSafeNormal();
	FVector AiTankForward = GetOwner()->GetActorForwardVector().GetSafeNormal();

	float rightThrow = FVector::CrossProduct(AiForwardIntention, AiTankForward).Z;
	IntendTurnRight(rightThrow);

	float forwardThrow = FVector::DotProduct(AiForwardIntention, AiTankForward);
	IntendMoveForward(forwardThrow);
}

void UTankMovementComponent::IntendMoveForward(float controlThrow)
{
	if (!ensure(leftTrack && rightTrack)) { return; }
	leftTrack->SetThrottle(controlThrow);
	rightTrack->SetThrottle(controlThrow);

	// #todo prevent double-speed 
}

void UTankMovementComponent::IntendTurnRight(float controlThrow)
{
	if (!ensure(leftTrack && rightTrack)) { return; }
	leftTrack->SetThrottle(controlThrow);
	rightTrack->SetThrottle(-controlThrow);

	// #todo prevent double-speed 
}