// Fill out your copyright notice in the Description page of Project Settings.

#include "TankTurret.h"

void UTankTurret::Rotate(float relativeSpeed)
{
	relativeSpeed = FMath::Clamp<float>(relativeSpeed, -1.0f, 1.0f);
	float rotationChange = relativeSpeed * m_maxDegreesPerSecond * GetWorld()->DeltaTimeSeconds;
	float rotation = RelativeRotation.Yaw + rotationChange;
	
	SetRelativeRotation(FRotator(0.0f, rotation, 0.0f));
}
