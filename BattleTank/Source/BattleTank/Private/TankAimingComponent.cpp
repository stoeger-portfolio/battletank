// Copyright Bernhard St�ger


#include "TankAimingComponent.h"
#include "Kismet/GameplayStatics.h"
#include "TankBarrel.h"
#include "Projectile.h"
#include "TankTurret.h"

// Sets default values for this component's properties
UTankAimingComponent::UTankAimingComponent()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;
}

void UTankAimingComponent::BeginPlay()
{
	// So that first fire is after initial reload
	m_lastFileTime = FPlatformTime::Seconds();
	m_currentAmmo = m_maxAmmo;
}

void UTankAimingComponent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);
	if (m_currentAmmo == 0)
	{
		m_firingState = EFiringState::OutOfAmmo;
	}
	else if ((FPlatformTime::Seconds() - m_lastFileTime) < m_reloadTimeInSeconds)
	{
		m_firingState = EFiringState::Reloading;
	}
	else if (IsBarrelMoving())
	{
		m_firingState = EFiringState::Aiming;
	}
	else {
		m_firingState = EFiringState::Locked;
	}
}

EFiringState UTankAimingComponent::GetFiringState()
{
	return m_firingState;
}

int UTankAimingComponent::GetCurrentAmmo() const
{
	return m_currentAmmo;
}

void UTankAimingComponent::AimAt(FVector hitLocation)
{
	if (!ensure(m_barrel)) { return; }

	FVector launchVelocity(0.0f);
	FVector startLocation = m_barrel->GetSocketLocation(FName("Projectile"));
	if (UGameplayStatics::SuggestProjectileVelocity(this, launchVelocity, startLocation, hitLocation, m_launchSpeed, false, 0.0f, 0.0f, ESuggestProjVelocityTraceOption::DoNotTrace))
	{
		m_aimDirection = launchVelocity.GetSafeNormal();
		MoveBarrelTowards(m_aimDirection);

		float time = GetWorld()->GetTimeSeconds();
	}
}

void UTankAimingComponent::Initialise(UTankBarrel* barrelToSet, UTankTurret* turretToSet)
{
	m_barrel = barrelToSet;
	m_turret = turretToSet;
}

void UTankAimingComponent::MoveBarrelTowards(FVector aimDirection)
{
	if (!ensure(m_barrel) || !ensure(m_turret)) { return; }
	FRotator barrelRotator = m_barrel->GetForwardVector().Rotation();
	FRotator aimAsRotator = aimDirection.Rotation();
	FRotator deltaRotator = aimAsRotator - barrelRotator;

	m_barrel->Elevate(deltaRotator.Pitch);
	if (deltaRotator.Yaw < FMath::Abs(180.0f))
	{
		m_turret->Rotate(deltaRotator.Yaw);
	}
	else {
		m_turret->Rotate(-deltaRotator.Yaw);
	}
}

bool UTankAimingComponent::IsBarrelMoving()
{
	if (!ensure(m_barrel)) { return false; }
	return !m_barrel->GetForwardVector().Equals(m_aimDirection, 0.01f);
}

void UTankAimingComponent::Fire()
{
	if (m_firingState == EFiringState::Locked || m_firingState == EFiringState::Aiming)
	{
		if (!ensure(m_barrel && m_projectile)) { return; }
		AProjectile* projectile = GetWorld()->SpawnActor<AProjectile>(m_projectile, m_barrel->GetSocketLocation(FName("Projectile")), m_barrel->GetSocketRotation(FName("Projectile")));

		projectile->LaunchProjectile(m_launchSpeed);
		m_lastFileTime = FPlatformTime::Seconds();
		--m_currentAmmo;
	}
}
