  // Fill out your copyright notice in the Description page of Project Settings.

#include "TankTrack.h"
#include "SprungWheel.h"
#include "SpawnPoint.h"

UTankTrack::UTankTrack()
{
	PrimaryComponentTick.bCanEverTick = true;
	SetNotifyRigidBodyCollision(true);
}

void UTankTrack::SetThrottle(float throttle)
{
	float currentThrottle = FMath::Clamp<float>(throttle + throttle, -1.0f, 1.0f);
	DriveTrack(currentThrottle);
}

void UTankTrack::DriveTrack(float throttle)
{
	float forceApplied = throttle * m_maximumDrivingForce;
	TArray<ASprungWheel*> wheels = GetWheels();
	float forcePerWheel = forceApplied / wheels.Num();

	for (ASprungWheel* wheel : wheels)
	{
		wheel->AddDrivingForce(forcePerWheel);
	}
}

TArray<ASprungWheel*> UTankTrack::GetWheels() const
{
	TArray<ASprungWheel*> result;
	TArray<USceneComponent*> children;
	GetChildrenComponents(true, children);

	for (USceneComponent* child : children)
	{
		USpawnPoint* spawnPointChild = Cast<USpawnPoint>(child);
		if (!spawnPointChild) { continue; }

		AActor* spawnedChild = spawnPointChild->GetSpawnedActor();
		ASprungWheel* sprungWheel = Cast<ASprungWheel>(spawnedChild);
		if (!sprungWheel) { continue; }

		result.Add(sprungWheel);
	}

	return result;
}
